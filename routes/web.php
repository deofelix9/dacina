<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Auth::routes();
Auth::routes(['register' => false]);
Route::get('/', function () {
    return view('auth.login');
});


    //contact us
  Route::get('/contact-us','contactusController@index');
  Route::post('/reply/{id}','contactusController@reply');
     

  //information
  Route::get('/info','informationController@getInfo');
  
  Route::post('/add-info','informationController@getInfoAdd');
  Route::post('/edit-info/{id}','informationController@getInfoEdit');

  //FAQ
  Route::get('/faq','faqController@getFaq');
  Route::post('/edit-faq/{id}','faqController@getfaqEdit');
  Route::post('/add-faq','faqController@getfaqAdd');
  
  //information category
  Route::get('/info-category','informationCategoryController@index');
  Route::post('/add-category','informationCategoryController@addCategory');
  Route::post('/edit-info-category/{id}','informationCategoryController@editCategory');

   //disruption category
   Route::get('/disruption-category','disruptionCategoryController@index');
   Route::post('/add-disruption-category','disruptionCategoryController@addDisruptionCategory');
   Route::post('/edit-disruption-category/{id}','disruptionCategoryController@editDisruptionCategory');

  //service disruption
  Route::get('/disruption','serviceDisruptionController@index');
  Route::post('/add-disruption','serviceDisruptionController@disruptionAdd');
  Route::post('/edit-disruption/{id}','serviceDisruptionController@disruptionEdit');

  //incidence
  Route::get('/incidence','incidenceController@index');
  Route::post('/incidence-reply/{id}','incidenceController@reply');

  Route::get('/brt-network','BrtNetworkController@index')->name('brt-network');
  Route::post('/brt-network/routes','BrtNetworkController@byRoute');
  Route::post('save-brt','BrtNetworkController@saveBrt');
  Route::post('update-sequence','BrtNetworkController@updateSequence');
  Route::get('/brt-network/routes','BrtNetworkController@viewByRoute');


//users

Route::middleware('can:accessAdmin')->group(function() {
    Route::get('/users','UsersController@index');
    // future adminpanel routes also should belong to the group
});

// Route::get('/users','UsersController@index');
Route::post('/users/add','UsersController@addUsers');
Route::post('/users/edit/{id}','UsersController@editUsers');
Route::post('/users/edit-role/{id}','UsersController@editUserRole');
Route::get('/users/activate/{id}','UsersController@Activate');
Route::get('/users/de-activate/{id}','UsersController@DeActivate');
Route::get('/users/reset-password/{id}','UsersController@resetPassword');

//dashboard
Route::get('/welcome','dashboardController@index');


//profile
Route::get('/profile','profileController@index');
Route::post('/edit-profile/{id}','profileController@editProfile');


//gtfs
Route::middleware('can:accessAdmin')->group(function() {
Route::get('/routes','gtfsController@getRoutes'); });
Route::get('/delete-routes','gtfsController@deleteRoutes');
Route::post('/upload/routes','gtfsController@uploadRoutes');

Route::get('/stop','gtfsController@getStops');
Route::get('/delete-stops','gtfsController@deleteStops');
Route::post('/upload/stops','gtfsController@uploadStops');

Route::get('/stop-times','gtfsController@getStopTimes');
Route::get('/delete-stop-times','gtfsController@deleteStopTimes');
Route::post('/upload/stop-times','gtfsController@uploadStopTimes');

Route::get('/trip','gtfsController@getTrips');
Route::get('/delete-trips','gtfsController@deleteTrips');
Route::post('/upload/trips','gtfsController@uploadTrips');

Route::get('/shapes','gtfsController@getShapes');
Route::get('/delete-shapes','gtfsController@deleteShapes');
Route::post('/upload/shapes','gtfsController@uploadShapes');

Route::get('/calendar','gtfsController@getCalendar');
Route::get('/delete-calendars','gtfsController@deleteCalendars');
Route::post('/upload/calendar','gtfsController@uploadCalendar');

Route::get('/calendar-dates','gtfsController@getCalendarDates');
Route::get('/delete-calendar-date','gtfsController@deleteCalendarDates');
Route::post('/upload/calendar-dates','gtfsController@uploadCalendarDates');

Route::get('/agency','gtfsController@getAgency');
Route::get('/delete-agency','gtfsController@deleteAgency');
Route::post('/upload/agency','gtfsController@uploadAgency');



// Route::get('/home', 'HomeController@index')->name('home');



