<!DOCTYPE html>
<html lang="en">
@extends('layouts.master')



  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            

            <div class="clearfix"></div>


        @include('sidebar')

        <!-- page content -->
        <div class="content">
        <div class="right_col" role="main">
          <div class="">
    

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Service Disruption</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>

<!-- add information -->
 <!-- Large modal -->
 <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target=".bs-example-modal-lg" >
 <i class="fa fa-plus-circle"></i> Add Service Disruption</button>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog ">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Add Service Disruption</h4>
      </div>
      <div class="modal-body">
      <form id="demo-form2" method="POST" action="/add-disruption" data-parsley-validate class="form-horizontal form-label-left">
@csrf
<div class="form-group">
  <label class="control-label" for="first-name">Title <span class="required">*</span>
  </label>
  <div class="form-group">
    <input type="text" id="title" name="title" required="required" class="form-control col-md-7 col-xs-12">
  </div>
</div>

<div class="form-group">

<label class="control-label" for="message">Description :</label>
<div class="form-group">
                          <textarea id="message" required="required" class="form-control" name="content" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.."
                            data-parsley-validation-threshold="10"></textarea>

  </div>
</div>

<div class="form-group">

<label class="control-label" for="last-name">Category <span class="required">*</span>
  </label>
                        <div class="form-group">
                          <select class="form-control" name='category' required>
                            <option value="">Choose option</option>
                            @foreach($dis_cat as $cat)
                            <option value="{{$cat->category_name}}">{{$cat->category_name}}</option>
                           @endforeach
                          </select>
                        </div>

  
</div>

<div class="form-group">
  <label class="control-label" for="first-name">Location <span class="required">*</span>
  </label>
  <div class="form-group">
    <input type="text" id="location" name="location" required="required" class="form-control col-md-7 col-xs-12">
  </div>
</div>

<div class="form-group">
  <label class="control-label" for="first-name">Summary <span class="required">*</span>
  </label>
  <div class="form-group">
    <input type="text" id="title" name="summary" required="required" class="form-control col-md-7 col-xs-12">
  </div>
</div>


<div class="ln_solid"></div>
<div class="form-group">
  <div class="form-group pull-right">
    
    <button class="btn btn-primary" type="reset">Reset</button>
    <button type="submit" class="btn btn-success">Submit</button>
  </div>
</div>

</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    
      </div>

    </div>
  </div>
</div>
<!-- add information -->







                  <div class="x_content table-responsive">
                  @if (session('status'))
                        <div class="alert alert-success "><i class='fa fa-check-circle'></i>
                            {{ session('status') }}
                        </div>
                        @endif

                        @if(session('error'))
                        <div class="alert alert-danger "><i class='fa fa-times-circle'></i>
                            {{ session('error') }}
                        </div>
                        @endif
                 <br>
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Title</th>
                          <th>Summary</th>
                          <th>Date Created</th>
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                      </thead>


                      <tbody>
                      @foreach($responses as $resp)
                        <tr>
                          <td>{{$loop->iteration}}</td>
                          <td>{{$resp->title}}</td>
                          <td>{{$resp->summary}}</td>
                         
                          <td>{{date('d/m/Y',strtotime($resp->createdAt))}}   </td>
                          <td>
                          @if($resp->status==1)
                             Active
                             @else
                             Inactive
                          @endif
                          </td>
                          <td>
                          <button type="button" class="btn btn-primary btn-xs" data-toggle="modal"  data-target="#{{$resp->id}}" ><i class="fa fa-folder"></i> View </button>
                          <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#{{$resp->id}}e"  ><i class="fa fa-pencil"></i> Edit </button>
                  

                          </td>
                        </tr>
                       
                        
                       <!-- view information -->
 <!-- Large modal -->
 

<div class="modal fade view-info" id= "{{$resp->id}}" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">View Service Disruption</h4>
      </div>
      <div class="modal-body">
     
      <p><strong>Title:</strong><br> {{$resp->title}}</p>
      <p><strong>Category:</strong><br> {{$resp->category}}</p> 
      <p><strong>Content:</strong><br> {{$resp->information}}</p> 
      <p><strong>Summary:</strong><br> {{$resp->summary}}</p>           
      <p><strong>Location:</strong><br> {{$resp->location}}</p>  
      <p><strong>Date Created:</strong><br> {{date('d/m/Y',strtotime($resp->createdAt))}}</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    
      </div>

    </div>
  </div>
</div>
<!-- view information -->


<!-- edit information -->
 <!-- Large modal -->

<div class="modal fade edit-info" id= "{{$resp->id}}e" tabindex="-1" role="dialog" aria-hidden="true">
  
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Edit Service Disruption</h4>
      </div>
      <div class="modal-body">
      <form id="demo-form2" method="POST" action="/edit-disruption/{{$resp->id}}/" data-parsley-validate class="form-horizontal form-label-left">
@csrf
<div class="form-group">
  <label class="control-label" for="first-name">Title <span class="required">*</span>
  </label>
  <div class="form-group">
    <input type="text" id="title" value="{{$resp->title}}" name="title" required="required" class="form-control col-md-7 col-xs-12">
  </div>
</div>

<div class="form-group">
<label class="control-label" for="last-name">Category <span class="required">*</span>
  </label>
  <div class="form-group">
                          <select class="form-control" name='category' required>
                            <option value="">Choose Category</option>
                            @foreach($dis_cat as $cat)
                            <option value="{{$cat->category_name}}"@if($cat->category_name==$resp->category) selected @endif>{{$cat->category_name}}</option>
                           @endforeach
                          </select>
                   </div>
</div>

<div class="form-group">
<label class="control-label" for="message">Description :</label>
<div class="form-group">
  <textarea id="message" required="required" class="form-control" name="content">{{$resp->information}}</textarea>
  </div>
</div>


<div class="form-group">
  <label class="control-label" for="first-name">Location <span class="required">*</span>
  </label>
  <div class="form-group">
    <input type="text" id="location"  value="{{$resp->location}}" name="location" required="required" class="form-control col-md-7 col-xs-12">
  </div>
</div>

<div class="form-group">
  <label class="control-label " for="first-name">Summary <span class="required">*</span>
  </label>
  <div class="form-group">
    <input type="text" id="title" value="{{$resp->summary}}" name="summary" required="required" class="form-control col-md-7 col-xs-12">
  </div>
</div>

<div class="form-group">

<label class="control-label " for="last-name">Status <span class="required">*</span>
  </label>
<div class="form-group">
                          <select class="form-control" name='status' required>
                            <option value="">Choose Status</option>
                            <option value="1" @if($resp->status==1) selected @endif>Active</option>
                            <option value="0" @if($resp->status==0) selected @endif>Inactive</option>
                             
                          </select>
                        </div>
</div>

<div class="ln_solid"></div>
<div class="form-group">
  <div class="form-group pull-right">
    
    <button class="btn btn-primary" type="reset">Reset</button>
    <button type="submit" class="btn btn-success">Submit</button>
  </div>
</div>

</form>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
    
      </div>

    </div>
  </div>
</div>
<!-- edit information --> 
                        
                      
                       @endforeach
                       
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              
            </div>
          </div>
        </div>
        </div>
       
        <!-- /page content -->

     
      </div>
    </div>

    

  </body>

</html>