<!DOCTYPE html>
<html lang="en">
@extends('layouts.master')



  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            

            <div class="clearfix"></div>


        @include('sidebar')

        <!-- page content -->
        <div class="content">
        <div class="right_col" role="main">
          <div class="">
    

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>FAQ </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                    </ul>
                    <div class="clearfix"></div>
                  </div>

<!-- add information -->
 <!-- Large modal -->
 <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target=".bs-example-modal-lg" >
<i class="fa fa-plus-circle"></i></span> Add FAQ</button>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Add FAQ</h4>
      </div>
      <div class="modal-body">
      <form id="demo-form2" method="POST" action="/add-faq" data-parsley-validate class="form-horizontal form-label-left">
@csrf
<div class="form-group">
  <label class="control-label" for="Question">Question <span class="required">*</span>
  </label>
  <div class="form-group">

<textarea id="question" name="question" required="required" class="form-control" rows="3" required></textarea>

  </div>
</div>
<div class="form-group">

<label class="control-label " for="last-name">Answer <span class="required">*</span>
  </label>
                        <div class="form-group">
                          <textarea id='answer' name='answer' required class="form-control" rows="9" required></textarea>
                        </div>

  
</div>
 

<div class="ln_solid"></div>
<div class="form-group">
  <div class="form-group pull-right">
    
    <button class="btn btn-primary" type="reset">Reset</button>
    <button type="submit" class="btn btn-success">Submit</button>
  </div>
</div>

</form>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
    
      </div>

    </div>
  </div>
</div>
<!-- add information -->







                  <div class="x_content table-responsive">
                  @if (session('status'))
                        <div class="alert alert-success "><i class='fa fa-check-circle'></i>
                            {{ session('status') }}
                        </div>
                        @endif

                        @if(session('error'))
                        <div class="alert alert-danger "><i class='fa fa-times-circle'></i>
                            {{ session('error') }}
                        </div>
                        @endif
                 <br>
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Question</th>
                          <th> Answer</th>
                          <th>created At</th>
                           <th>Status</th>
                          <th>Action</th>
                        </tr>
                      </thead>


                      <tbody>
                      @foreach($responses as $resp)
                        <tr>
                          <td>{{$resp->question}}</td>
                          <td>{{$resp->answer}}</td>
                          <td>{{date('d/m/Y',strtotime($resp->createdAt))}}   </td>
                           <td>
                           @if($resp->status==1)
                             Active
                             @else
                             Inactive
                          @endif
                          <td>
                          
                          <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#{{$resp->id}}e"  ><i class="fa fa-pencil"></i> Edit </button>
                  

                          </td>
                        </tr>
                       
                        
                       <!-- view information -->
 
<!-- edit information -->
 <!-- Large modal -->

<div class="modal fade edit-faq" id= "{{$resp->id}}e" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Edit FAQ</h4>
      </div>
      <div class="modal-body">
      <form id="demo-form2" method="POST" action="/edit-faq/{{$resp->id}}/" data-parsley-validate class="form-horizontal form-label-left">
@csrf
<div class="form-group">
  <label class="control-label" for="question"> Question <span class="required">*</span>
  </label>
  <div class="form-group">
    <textarea id="question" name="question" required="required" class="form-control" rows="3">{{$resp->question}}</textarea>
  </div>
</div>
<div class="form-group">
  <label class="control-label " for="answer"> Answer <span class="required">*</span>
  </label>
  <div class="form-group">
    <textarea id="answer"  name="answer" required="required" class="form-control" rows="9">{{$resp->answer}}</textarea>
  </div>
</div>


<div class="form-group">

<label class="control-label " for="last-name">Status <span class="required">*</span>
  </label>
  <div class="form-group">
                          <select class="form-control" name='status' required>
                            <option value="">Choose Status</option>
                            <option value="1" @if($resp->status==1) selected @endif>Active</option>
                            <option value="0" @if($resp->status==0) selected @endif>Inactive</option>
                             
                          </select>
                        </div>
</div>
 

<div class="ln_solid"></div>
<div class="form-group">
  <div class="form-group pull-right">
    
    <button class="btn btn-primary" type="reset">Reset</button>
    <button type="submit" class="btn btn-success">Submit</button>
  </div>
</div>

</form>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
    
      </div>

    </div>
  </div>
</div>
<!-- edit information --> 
                        
                      
                       @endforeach
                       
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              
            </div>
          </div>
        </div>
        </div>
       
        <!-- /page content -->

     
      </div>
    </div>

    

  </body>

</html>