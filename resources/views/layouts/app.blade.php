<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<style type="text/css">
		
 
</style>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{'DAR City Navigator' }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
	 
	<link rel="icon" type="image/png" href="favicon.ico">
 	
<link rel="stylesheet" type="text/css" href="{{ asset('css/vendor/animate/animate.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/fonts/Linearicons-Free-v1.0.0/icon-font.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/fonts/Linearicons-Free-v1.0.0/icon-font.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/vendor/css-hamburgers/hamburgers.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/util.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/main.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/vendor/select2/select2.min.css')}}">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body style='background-image: url("/bgg.png");background-repeat: no-repeat;background-size: cover;background-attachment: fixed; background-color:#f8fafc'>
     
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
