<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>DAR City Navigator </title>

    <!-- Bootstrap -->
    <link href="/gent/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/gent/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="/gent/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="/gent/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">


     <!-- iCheck -->
 <link href="/gent/vendors/iCheck/skins/flat/green.css" rel="stylesheet">

<!-- Datatables -->
<link href="/gent/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
  <link href="/gent/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
  <link href="/gent/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
  <link href="/gent/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
  <link href="/gent/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="/gent/build/css/custom.min.css" rel="stylesheet">
    
    <!-- Datatables-->
<link href="css/datatables.min.css" rel="stylesheet" />
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
            
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
        
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
                       <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
          
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
       
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
        @yield('content')
        </div>

        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
         {{date('Y')}}  &copy; DAR City Navigator App Portal
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="/gent/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="/gent/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="/gent/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="/gent/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="/gent/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- jQuery Sparklines -->
    <script src="/gent/vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- Flot -->
    <script src="/gent/vendors/Flot/jquery.flot.js"></script>
    <script src="/gent/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="/gent/vendors/Flot/jquery.flot.time.js"></script>
    <script src="/gent/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="/gent/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="/gent/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="/gent/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="/gent/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="/gent/vendors/DateJS/build/date.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="/gent/vendors/moment/min/moment.min.js"></script>
    <script src="/gent/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    
    <!-- Custom Theme Scripts -->
    <script src="/gent/build/js/custom.min.js"></script>


      <!-- iCheck -->
  <script src="/gent/vendors/iCheck/icheck.min.js"></script>
  <!-- Datatables -->
  <script src="/gent/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/gent/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="/gent/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="/gent/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="/gent/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="/gent/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="/gent/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="/gent/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="/gent/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="/gent/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="/gent/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="/gent/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="/gent/vendors/jszip/dist/jszip.min.js"></script>
    <script src="/gent/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="/gent/vendors/pdfmake/build/vfs_fonts.js"></script>