<!DOCTYPE html>
<html lang="en">
@extends('layouts.master')



  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            

            <div class="clearfix"></div>


        @include('sidebar')

        <!-- page content -->
        <div class="content">
        <div class="right_col" role="main">
          <div class="">
    

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Disruption Category </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>

<!-- add information -->
 <!-- Large modal -->
 <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target=".bs-example-modal-lg" >
 <i class="fa fa-plus-circle"></i> Add Category</button>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Add Category</h4>
      </div>
      <div class="modal-body">
      <form id="demo-form2" method="POST" action="/add-disruption-category" data-parsley-validate class="form-horizontal form-label-left">
@csrf
<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Category <span class="required">*</span>
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="text" id="title" name="category" required="required" class="form-control col-md-7 col-xs-12">
  </div>
</div>


<div class="ln_solid"></div>
<div class="form-group">
  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
    
    <button class="btn btn-primary" type="reset">Reset</button>
    <button type="submit" class="btn btn-success">Submit</button>
  </div>
</div>

</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    
      </div>

    </div>
  </div>
</div>
<!-- add information -->







                  <div class="x_content table-responsive">
                  @if (session('status'))
                        <div class="alert alert-success "><i class='fa fa-check-circle'></i>
                            {{ session('status') }}
                        </div>
                        @endif

                        @if(session('error'))
                        <div class="alert alert-danger "><i class='fa fa-times-circle'></i>
                            {{ session('error') }}
                        </div>
                        @endif
                 <br>
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Category Name</th>
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                      </thead>


                      <tbody>
                      @foreach($category as $cat)
                        <tr>
                          <td>{{$loop->iteration}}</td>
                          <td>{{$cat->category_name}}   </td>
                          <td>{{$cat->status}}   </td>
                          <td>
                          <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#{{$cat->id}}e"  ><i class="fa fa-pencil"></i> Edit </button>
                          

                          </td>
                        </tr>
                       
                        
                      
<!-- edit information -->
 <!-- Large modal -->

<div class="modal fade edit-info" id= "{{$cat->id}}e" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Edit Category</h4>
      </div>
      <div class="modal-body">
      <form id="demo-form2" method="POST" action="/edit-disruption-category/{{$cat->id}}/" data-parsley-validate class="form-horizontal form-label-left">
@csrf
<div class="form-group">
  <label class="control-label " for="first-name">Category<span class="required">*</span>
  </label>
  <div class="form-group">
    <input type="text" id="category" value="{{$cat->category_name}}" name="category" required="required" class="form-control col-md-7 col-xs-12">
  </div>
</div>
<div class="form-group">

<label class="control-label " for="last-name">Status <span class="required">*</span>
  </label>
  <div class="form-group">
                          <select class="form-control" name='status'>
                            <option>Choose Status</option>
                            <option value="Active" @if($cat->status=='Active') selected @endif >Active</option>
                            <option value="InActive" @if($cat->status=='InActive') selected @endif >InActive</option>
                          </select>

                     
                        </div>

  
</div>


<div class="ln_solid"></div>
<div class="form-group">
  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
    
    <button class="btn btn-primary" type="reset">Reset</button>
    <button type="submit" class="btn btn-success">Submit</button>
  </div>
</div>

</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    
      </div>

    </div>
  </div>
</div>
<!-- edit information --> 
                        
                      
                       @endforeach
                       
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              
            </div>
          </div>
        </div>
        </div>
       
        <!-- /page content -->

     
      </div>
    </div>

    

  </body>

</html>