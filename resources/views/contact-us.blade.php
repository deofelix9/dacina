<!DOCTYPE html>
<html lang="en">
@extends('layouts.master')



  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            

            <div class="clearfix"></div>


        @include('sidebar')

        <!-- page content -->
        <div class="content">
        <div class="right_col" role="main">
          <div class="">
    

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Contact us </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>





                  <div class="x_content table-responsive">
                  @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                        @endif
                 <br>
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Commuter  Name</th>
                          <th>Phone Number</th>
                          <th>Email</th>
                          <th>Message date</th>
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                      </thead>


                      <tbody>
                      @foreach($responses as $resp)
                        <tr>
                          <td>{{$loop->iteration}}</td>
                          <td>{{$resp->name}}</td>
                          <td>{{$resp->msisdn}}</td>
                          <td>{{$resp->email}}</td>
                          
                          <td>{{date('d/m/Y',strtotime($resp->createdAt))}}   </td>
                          <td>
                         @if($resp->status==1)
                         
                         <span class="label label-success"><i class="fa fa-refresh"></i> Replied</span>
                         @else
                         <span class="label label-danger"><i class="fa fa-refresh"></i> Unreplied</span>
                         @endif
                              </td>
                          <td>
                          <button type="button" class="btn btn-primary btn-xs" data-toggle="modal"  data-target="#{{$resp->id}}" ><i class="fa fa-folder"></i> View </button>
                    @empty(!$resp->msisdn && !$resp->email)
                          <button type="button" class="btn btn-dark btn-xs" data-toggle="modal"  data-target="#{{$resp->id}}rep" ><i class="fa fa-reply"></i> Reply </button>
                         @endempty
                          </td>
                        </tr>
                       
                        
                       <!-- view information -->
 <!-- Large modal -->
 

<div class="modal fade view-info" id= "{{$resp->id}}" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">View Message</h4>
      </div>
      <div class="modal-body">
     
      <p><strong>Commuter Name:</strong><br> {{$resp->name}}</p>
      <p><strong>Phone Number:</strong><br> {{$resp->msisdn}}</p>
      <p><strong>Email:</strong><br> {{$resp->email}}</p>
      <p><strong>Content:</strong><br> {{$resp->content}}</p>
      <p><strong> Message Date</strong><br> {{date('d/m/Y',strtotime($resp->createdAt))}}</p> 
             

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    
      </div>

    </div>
  </div>
</div>
<!-- view information -->



 <!-- Add Reply -->
 <!-- Large modal -->
 

 <div class="modal fade view-info" id= "{{$resp->id}}rep" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">View Message</h4>
      </div>
      <div class="modal-body">
     
      <p><strong>Commuter Name:</strong><br> {{$resp->name}}</p>
      <p><strong>Phone Number:</strong><br> {{$resp->msisdn}}</p>
      <p><strong>Email:</strong><br> {{$resp->email}}</p>
      <p><strong>Content:</strong><br> {{$resp->content}}</p>
      <p><strong> Message Date</strong><br> {{date('d/m/Y',strtotime($resp->createdAt))}}</p> 
             

    


      <form method="POST" action="/reply/{{$resp->id}}/" accept-charset="UTF-8" class="add_p"><input name="_token" type="hidden" value="SXDVCVBsVUfkBm8NiKeEFDn3Zm1CrBvOfEgT3NRk">
                    <fieldset>
                    @csrf
                        <div class="form-group">
                            <label>Reply</label>
                            <textarea class="form-control" id="editor{{$resp->id}}m" name="message"  required> </textarea>
                        </div>
                        
                    <input type="hidden" name='email' value='{{$resp->email}}'>
                    <input type="hidden" name='phone' value='{{$resp->msisdn}}'>
  
                        <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-reply"></i> Reply</button>
                    </fieldset>
               </form>



               </div>


      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    
      </div>

    </div>
  </div>
</div>
<!-- Add Reply -->



                      
                       @endforeach
                       
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              
            </div>
          </div>
        </div>
        </div>
       
        <!-- /page content -->

     
      </div>
    </div>

    

  </body>

</html>