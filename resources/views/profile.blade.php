<!DOCTYPE html>
<html lang="en">
@extends('layouts.master')



  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            

            <div class="clearfix"></div>


        @include('sidebar')

        <!-- page content -->
        <div class="content">
        <div class="right_col" role="main">
          <div class="">
    

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>User Profile </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>








                  <div class="x_content">
                  @foreach ($errors->all() as $error)
                            <div class="alert alert-danger">{{ $error }}</div>
                         @endforeach 
                  @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                        @endif
                 <br>

                <div class="card">
                <h2>{{$user->name}}</h2>
  <img src="/gent/images/profile.jpg" alt="pic" style="width:10%">
  <p></p>
  <p class="title"><i class="fa fa-user"></i> <strong>Full Name:</strong> {{$user->name}}</p>
  <p><i class="fa fa-envelope"></i> <strong>Email: </strong> {{$user->email}}</p>
  <p><i class="fa fa-key"></i> <strong>Role:</strong>  {{$user->role}}</p>
  <p> <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#{{$user->id}}e"  ><i class="fa fa-pencil"></i> Change Password</button></p>
</div>
                      

<!-- edit information -->
 <!-- Large modal -->

<div class="modal fade edit-info" id= "{{$user->id}}e" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Change Password</h4>
      </div>
      <div class="modal-body">
      <form id="demo-form2" method="POST" action="/edit-profile/{{$user->id}}/" data-parsley-validate class="form-horizontal form-label-left">
@csrf
<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Current Password <span class="required">*</span>
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="password" id="title"  name="currentPassword" required="required" class="form-control col-md-7 col-xs-12">
  </div>
</div>
<div class="form-group">

<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">New Password <span class="required">*</span>
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
  <input type="password" id="title"  name="newPassword" required="required" class="form-control col-md-7 col-xs-12">
                        </div>

  
</div>
<div class="form-group">

<label class="control-label col-md-3 col-sm-3 col-xs-12" for="message">Confirm Password :</label>
<div class="col-md-6 col-sm-6 col-xs-12">
<input type="password" id="title"  name="confirmPassword" required="required" class="form-control col-md-7 col-xs-12">

  </div>
</div>

<div class="ln_solid"></div>
<div class="form-group">
  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
    
    <button class="btn btn-primary" type="reset">Reset</button>
    <button type="submit" class="btn btn-success">Submit</button>
  </div>
</div>

</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    
      </div>

    </div>
  </div>
</div>
<!-- edit information --> 
                        
                      
                    
                  </div>
                </div>
              </div>

              
            </div>
          </div>
        </div>
        </div>
       
        <!-- /page content -->

     
      </div>
    </div>

    

  </body>

</html>