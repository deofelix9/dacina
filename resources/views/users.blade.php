            <!DOCTYPE html>
            <html lang="en">
            @extends('layouts.master')

              <body class="nav-md">
                <div class="container body">
                  <div class="main_container">
                    <div class="col-md-3 left_col">
                      <div class="left_col scroll-view">
                        

                        <div class="clearfix"></div>


                    @include('sidebar')

                    <!-- page content -->
                    <div class="content">
                    <div class="right_col" role="main">
                      <div class="">
                

                        <div class="clearfix"></div>

                        <div class="row">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                              <div class="x_title">
                                <h2>Users </h2>
                                <ul class="nav navbar-right panel_toolbox">
                                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                  </li>
                                </ul>
                                <div class="clearfix"></div>
                              </div>

            <!-- add user -->
            <!-- Large modal -->
            <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target=".bs-example-modal-lg" >
            <i class="fa fa-plus-circle"></i> Add Users</button>

            <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog ">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Add User</h4>
                  </div>
                  <div class="modal-body">
                  <form id="demo-form2" method="POST" action="/users/add" data-parsley-validate class="form-horizontal form-label-left">
            @csrf
            <div class="form-group">
              <label class="control-label" for="first-name">Full Name <span class="required">*</span>
              </label>
              <div class="form-group">
                <input type="text" id="name" name="name" required="required" class="form-control col-md-7 col-xs-12">
              </div>
            </div>
            <div class="form-group">

            <label class="control-label" for="last-name">Role <span class="required">*</span>
              </label>
                                    <div class="form-group">
                                      <select class="form-control" name='role' required>
                                        <option value="">Choose Role</option>
                                        <option value="User">User</option>
                                        <option value="Admin">Admin</option>
                                      </select>
                                    </div>

              
            </div>
            <div class="form-group">

            <label class="control-label" for="message">Email :</label>
            <div class="form-group">
            <input type="email" id="email" name="email" required="required" class="form-control col-md-7 col-xs-12">

              </div>
            </div>

            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="form-group pull-right">
                
                <button class="btn btn-primary" type="reset">Reset</button>
                <button type="submit" class="btn btn-success">Submit</button>
              </div>
            </div>

            </form>
                  </div>
                  <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                
                  </div>

                </div>
              </div>
            </div>
            <!-- add user -->







                              <div class="x_content table-responsive">
                              @if (session('status'))
                                    <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                      <i class="fa fa-check-circle"></i>  {{ session('status') }}
                                    </div>

                                    @elseif (session('exist'))
                                    <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                      <i class="fa fa-times-circle"></i>  {{ session('exist') }}
                                    </div>
                                    @endif
                            <br>
                                <table id="datatable" class="table table-striped table-bordered">
                                  <thead>
                                    <tr>
                                    <td>#</td>
                            <td>Full Name</td>
                            <td>Email</td>
                            <th>Role</th>
                            <th>Status</th>
                              <th>Action</th>
                                    </tr>
                                  </thead>


                                  <tbody>
                                  @foreach($users as $user)
                                    <tr>
                                    <td>{{$loop->iteration}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->role}}</td>
                          @if($user->status=='Active')
                            <td>
                              <span class="label label-success"><i class="fa fa-refresh fa-spin"></i> Active</span>
                        
                              
                              </td>
                            
                            <td>
                                <div class="btn-group">
                                    <a href="/users/de-activate/{{$user->id}}"> <button type="button" class="btn btn-danger btn-xs"><i class='fa fa-lock'
                                    onclick = "return confirm('Are you sure you want to Lock the Account?');" ></i> Deactivate</button></a>
                                  
                                </div>
                
                                <div class="btn-group">
                    
                                <a href="/users/reset-password/{{$user->id}}">  <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#{{$user->id}}e"  
                                onclick = "return confirm('Are you sure you want to Reset the password?');" ><i class="fa fa-pencil"></i> Reset Password </button></a>
                                </div>
                              

                                <div class="exx btn-group" id="ex">
                    
                     <!-- <a href="/users/edit-role/{{$user->id}}">  -->
                    <button type="button"  data-toggle="modal"  data-target="#{{$user->id}}er" 
                    class="btn btn-primary btn-xs" data-toggle="modal"   >
                    <i class="fa fa-pencil"></i> Change Role </button>
                    <!-- </a> -->
                    </div>

                            </td>
                        
                                @else
                                <td><span class="label label-danger"><i class="fa fa-refresh"></i> Inactive</span>
                              
                              </td>
                              <td>
                                <div class="btn-group">
                                    <a href="/users/activate/{{$user->id}}"> <button type="button" class="btn btn-success btn-xs" 
                                    onclick = "return confirm('Are you sure you want to Activate the Account?');" ><i class='fa fa-unlock'></i> Activate</button></a>
                                  
                                </div>
                
                                <div class="btn-group">
                    
                                <a href="/users/reset-password/{{$user->id}}">  <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#{{$user->id}}e" 
                                onclick = "return confirm('Are you sure you want to Reset the password?');"  ><i class="fa fa-pencil"></i> Reset Password </button></a>
                                </div>


                                    <div class="btn-group">
                    
                                <a href="/users/reset-password/{{$user->id}}">  <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#{{$user->id}}e"   onclick = "return confirm('Are you sure you want to Reset the password?');"  ><i class="fa fa-pencil"></i> Reset Password </button></a>
                                </div>
              
                            </td>
            @endif

                      

                          </td>
                                      
                                    </tr>
                                  

                                 <!-- edit user -->
 <!-- Large modal -->

<div class="modal fade edit-info" id= "{{$user->id}}er" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Edit Role for {{$user->name}}</h4>
      </div>
      <div class="modal-body">
      <form id="demo-form2" method="POST" action="/users/edit-role/{{$user->id}}/" data-parsley-validate class="form-horizontal form-label-left">
@csrf

<div class="form-group">
<label class="control-label " for="last-name">Role <span class="required">*</span>
  </label>
  <div class="form-group">
                          <select class="form-control" name='role' required>
                            <option value="">Choose Role</option>
                         
                            <option value="Admin" @if($user->role=='Admin') selected @endif>Admin</option>
                            <option value="User" @if($user->role=='User') selected @endif>User</option>
                             
                          </select>
                        </div>
</div>




<div class="ln_solid"></div>
<div class="form-group">
  <div class="form-group pull-right">
    
    <button class="btn btn-primary" type="reset">Reset</button>
    <button type="submit" class="btn btn-success">Submit</button>
  </div>
</div>

</form>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
    
      </div>

    </div>
  </div>
</div>
<!-- edit information -->    
                                  
                                  @endforeach
                                  
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>

                          
                        </div>
                      </div>
                    </div>
                    </div>
                  
                    <!-- /page content -->

                
                  </div>
                </div>

            

              </body>

            </html>