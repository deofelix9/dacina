@extends('layouts.master')
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>DAR City Navigator </title>

    
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
             

            <div class="clearfix"></div>

        @include('sidebar')

        <!-- page content -->
        <div class="content">
        <div class="right_col" role="main">
          <div class="">
            <div class="row top_tiles ">
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12 ">
                <div class="tile-stats ">
                  <div class="icon " ><i class="fa fa-ban text-danger "></i></div>
                  <div class="count">{{$disCount}}</div>
                  <h3 class="text-danger">Disruption</h3>
                  <p >Service Disruption</p>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon "><i class="fa fa-bell text-info"></i></div>
                  <div class="count">{{$infoCount}}</div>
                  <h3>Information</h3>
                  <p>Information</p>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-cogs text-success"></i></div>
                  <div class="count">{{$faqCount}}</div>
                  <h3>FAQ</h3>
                  <p>Frequent Asked Question.</p>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats" >
                  <div class="icon"><i class="fa fa-exclamation-triangle text-warning"></i></div>
                  <div class="count">{{$incCount}}</div>
                  <h3>Incidence</h3>
                  <p>Incidence from commuters</p>
                </div>
              </div>
            </div>

            


            


            <div class="row">
              <div class="col-md-4">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Disruption</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <div class="dashboard-widget-content">

                    <ul class="list-unstyled timeline widget">
                      <li>
                        @foreach ($disruption as $dis)
                        <div class="block">
                          <div class="block_content">
                            <h2 class="title">
                                              <a>{{$dis->title}}&nbsp;</a>
                                          </h2>
                            <div class="byline">
                              <span>{{date('d/m/Y',strtotime($dis->createdAt))}}</a>
                            </div>
                            <p class="excerpt"> {{$dis->summary}}
                            </p>
                          </div>
                        </div>
                       @endforeach
                      </li>
                     
                      
                    </ul>
                  </div>
                  @if(count($disruption) >0 )
                  <a href="/disruption" class="btn btn-xs btn-success pull-right" role="button" aria-pressed="true"><i class="fa fa-eye"></i> View all </a>
                  @else
                 No Content
                  @endif
                </div>
              </div>
            </div>

              <div class="col-md-4">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Information </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <div class="dashboard-widget-content">

                    <ul class="list-unstyled timeline widget">
                      <li>
                        @foreach($information as $info)
                        <div class="block">
                          <div class="block_content">
                            <h2 class="title">
                                              <a>{{$info->title}}</a>
                                          </h2>
                            <div class="byline">
                            <span>{{date('d/m/Y',strtotime($info->createdAt))}}</a>
                            </div>
                            <p class="excerpt">{{$info->content}}
                            </p>
                          </div>
                        </div>
                        @endforeach
                      </li>
                      
                    </ul>
                  </div>
                  @if(count($information)>0)
                  <a href="/info" class="btn btn-xs btn-success pull-right"><i class="fa fa-eye"></i> View all </a>
                 @else
                 No Content
                  @endif
                </div>
              </div>
            </div>
              <div class="col-md-4">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Incidence </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <div class="dashboard-widget-content">

                    <ul class="list-unstyled timeline widget">
                      <li>
                        @foreach($incidence as $inc)
                        <div class="block">
                          <div class="block_content">
                            <h2 class="title">
                                              <a>{{$inc->incidentLocation}}</a>
                                          </h2>
                            <div class="byline">
                              <span>{{date('d/m/Y',strtotime($inc->createdAt))}}</a>
                            </div>
                            <p class="excerpt">{{$inc->incidentDescription}}</a>
                            </p>
                          </div>
                        </div>
                        @endforeach
                      </li>
                      
                    </ul>
                  </div>
                  @if(count($incidence)>0)
                  <a href="/incidence" class="btn btn-xs btn-success pull-right"><i class="fa fa-eye"></i> View all </a>
                  @else
                 No Content
                  @endif
                </div>
              </div>
            </div>
            </div>
          </div>
        </div>
        </div>
        <!-- /page content -->

       
      </div>
    </div>

   
  </body>
</html>