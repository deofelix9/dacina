<!DOCTYPE html>
<html lang="en">
@extends('layouts.master')

<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="clearfix"></div>


                    @include('sidebar')

                    <!-- page content -->
                    <div class="content">
                        <div class="right_col" role="main">


                            <div class="row">
                                <div class="col-md-12">
                                    <h2>Routes </h2>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="x_panel">
                                                <div class="x_title">
                                                    <h2>Points</h2>
                                                </div>
                                                <div class="x_content">
                                                    <div class="dashboard-widget-content">

                                                        <ul class="list-unstyled timeline widget">
                                                            <li>
                                                                @foreach ($points as $p)
                                                                <div class="block">
                                                                    <div class="block_content">
                                                                        <h2 class="title">
                                                                            {{$p->sequence}}. {{$p->stopName}} (ID
                                                                            {{$p->id}}
                                                                            | POINTS
                                                                            {{$p->latitude}},{{$p->longitude}}
                                                                            )
                                                                        </h2>
                                                                        <div class="byline">
                                                                            <span>{{$p->description}}</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                @endforeach
                                                            </li>


                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <h2>Add Point</h2>
                                            <form class="form-horizontal" action="/save-brt" method="POST">
                                                @csrf
                                                <div class="form-group">
                                                    <label class="control-label col-sm-2" for="description">Description</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control" id="description" name="description"
                                                            placeholder="Enter description">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-2" for="direction">Direction</label>
                                                    <div class="col-sm-10">
                                                        <select name="direction" id="direction"  class="form-control">
                                                            <option value="1">1</option>
                                                            <option value="0">0</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-2" for="latitude">Latitude</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control" id="latitude" name="latitude">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-2" for="longitude">Longitude</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control" id="longitude" name="longitude">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-2" for="pointType">Point Type</label>
                                                    <div class="col-sm-10">
                                                        <select id="pointType" name="pointType" class="form-control">
                                                            <option value="99">Normal Point</option>
                                                            <option value="3">Terminal</option>
                                                            <option value="1">Dart stop</option>
                                                            <option value="2">Feeder stop</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-2" for="stopName">Stop name</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control" id="stopName" name="stopName"
                                                        >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-2" for="pwd">Sequence</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control" id="sequence" name="sequence">
                                                    </div>
                                                </div>
                                                <input value="{{$routeId}}" name="routeId" type="hidden">
                                                <div class="form-group">
                                                    <div class="col-sm-offset-2 col-sm-10">
                                                        <button type="submit" class="btn btn-default">Submit</button>
                                                    </div>
                                                </div>
                                            </form>
                                            <br><br><br>
                                            <h2>Update Sequence</h2>
                                            <form class="form-horizontal" action="/update-sequence" method="POST">
                                                @csrf
                                                <div class="form-group">
                                                    <label class="control-label col-sm-2" for="sequence1">Point</label>
                                                    <div class="col-sm-10">
                                                        <select name="id" class="form-control">
                                                            <option value=""></option>
                                                            @foreach ($points as $p)
                                                            <option value="{{$p->id}}">{{$p->stopName}} {{$p->latitude}},{{$p->longitude}} ({{$p->description}})</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-2" for="sequence1">Sequence</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control" id="sequence1" name="sequence"
                                                            placeholder="Enter desired sequence number">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-offset-2 col-sm-10">
                                                        <button type="submit" class="btn btn-default">Submit</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>

                                    </div>




                                </div>
                            </div>

                        </div>

                        <!-- /page content -->


                    </div>
                </div>



</body>

</html>