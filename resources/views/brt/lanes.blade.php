<!DOCTYPE html>
<html lang="en">
@extends('layouts.master')
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="clearfix"></div>


        @include('sidebar')

        <!-- page content -->
        <div class="content">
        <div class="right_col" role="main">
          <div class="">
    

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Routes </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>


                  <form action="/brt-network/routes" method="POST" >
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <select class="form-control" name="routeId">
                        <option value="">Choose Route</option>
                        @foreach($routes as $r)
                        <option value="{{$r->id}}">{{$r->routeLongName}} - {{$r->routeShortName}}</option>
                        @endforeach
                    </select>

                </div>

   
                <div class="col-md-6">
                    <button type="submit" class="btn btn-success"><i class='fa fa-eye'></i> View</button>
                </div>
   
            </div>

            
        </form>


        <div class="x_content table-responsive">
                  @if (session('status'))
                        <div class="alert alert-success "><i class='fa fa-check-circle'></i>
                            {{ session('status') }}
                        </div>
                        @endif

                        @if(session('error'))
                        <div class="alert alert-danger "><i class='fa fa-times-circle'></i>
                            {{ session('error') }}
                        </div>
                        @endif
                 <br>

                  </div>
                </div>
              </div>

              
            </div>
          </div>
        </div>
        </div>
       
        <!-- /page content -->

     
      </div>
    </div>

    

  </body>

</html>