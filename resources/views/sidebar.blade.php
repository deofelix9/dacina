        <!-- menu profile quick info -->
        <div class="profile clearfix">
              <div class="logo">
              <a href="/welcome" class="site_title"><image src='/gent/images/dartlogo.png' style="border-radius:;background:;"> </a>
              </div>

            </div>
            <!-- /menu profile quick info -->

      <!-- sidebar menu -->
      <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>DAR CITY NAVIGATOR</h3>
                <br>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-ban"></i> Service Disruption <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">

                      <li><a href="/disruption">Service Disruption</a></li>
                      <li><a href="/disruption-category">Disruption Category</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-info-circle"></i> Information <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/info">Information</a></li>
                      <li><a href="/info-category">Information Category</a></li>
                    </ul>
                  </li>

                  <li><a><i class="fa fa-cogs"></i> Incidence <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/incidence">Incidence</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-comments"></i> FAQ <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/faq">FAQ</a></li>

                    </ul>
                  </li>
                  <li><a><i class="fa fa-phone"></i> Contact Us <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/contact-us">Contact Us</a></li>
                    </ul>
                  </li>
                  @if(Auth::user()->role=='Admin')
                  <li><a><i class="fa fa-upload"></i> GTFS Data <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="#">Agency</a></li>
                      <!-- <li><a href="/agency">Agency</a></li> -->
                      <li><a href="/calendar">Calendar</a></li>
                      <li><a href="#">Calendar Dates</a></li>
                      <li><a href="/routes">Routes</a></li>
                      <li><a href="/shapes">Shapes</a></li>
                      <li><a href="stop-times">Stop Times</a></li>
                      <li><a href="/stop">Stops</a></li>
                      <li><a href="/trip">Trips</a></li>
                    </ul>
                  </li>

                  <li>
                  <a><i class="fa fa-upload"></i> BRT Network <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                  <li><a href="/brt-network">Lanes</a></li>
                  </ul>
                  </li>



                  <li><a><i class="fa fa-users"></i> Users <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                  <li><a href="/users">Users</a></li>



                  @endif
                    </ul>
                  </li>

                </ul>
              </div>


            </div>
            <!-- /sidebar menu -->

             <!-- /menu footer buttons -->
             <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Dashboard" href='/welcome'>
                <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Profile" href='/profile'>
                <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="{{ route('logout') }}"    onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle"   data-toggle="dropdown" aria-expanded="false">
                    <img src="images/img.jpg" alt="" > <font style='color:#fff'>

                    @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @else
                                    {{ Auth::user()->name }}
                                    @endguest


                </font>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="/profile"><i class="fa fa-user pull-right"></i> Profile</a></li>


                    <li><a href="{{ route('logout') }}"    onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
    </form>








                  </ul>
                </li>




                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
