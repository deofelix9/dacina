<!DOCTYPE html>
<html lang="en">
@extends('layouts.master')



  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            

            <div class="clearfix"></div>


        @include('sidebar')

        <!-- page content -->
        <div class="content">
        <div class="right_col" role="main">
          <div class="">
    

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Shapes </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>


                  <form action="/upload/shapes" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
  
                <div class="col-md-6">
                    <input type="file" name="file" class="form-control" required>
                </div>
   
                <div class="col-md-6">
                    <button type="submit" class="btn btn-success"><i class='fa fa-upload'></i> Upload</button>
                </div>
   
            </div>

           
        </form>

        <div class="pull-right">
                  <a href="/delete-shapes"><button type="submit" class="btn btn-danger"  onclick = "return confirm('Are you sure you want to delete all shapes?');"><i class='fa fa-trash'></i> Delet All</button>
                  </a>  
                </div>

                  <div class="x_content table-responsive">
                  @if (session('status'))
                        <div class="alert alert-success "><i class='fa fa-check-circle'></i>
                            {{ session('status') }}
                        </div>
                        @endif

                        @if(session('error'))
                        <div class="alert alert-danger "><i class='fa fa-times-circle'></i>
                            {{ session('error') }}
                        </div>
                        @endif
                 <br>
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Shape ID</th>
                          <th>Shape Latitude</th>
                          <th>Shape Longitude</th>
                          <th>Shape Sequence</th>
                          <th>Shape Dist Traveled</th>
                          <th>Created At</th>
        
                        </tr>
                      </thead>

                      <tbody>
                      @foreach($responses as $resp)
                        <tr>
                          <td>{{$loop->iteration}}</td>
                          <td>{{$resp->shapeId}}</td>
                          <td>{{$resp->shapePtLat}}</td>
                          <td>{{$resp->shapePtLon}}</td>
                          <td>{{$resp->shapePtSequence}}</td>
                          <td>{{$resp->shapeDistTraveled}}</td>
                          <td>{{date('d/m/Y',strtotime($resp->createdAt))}}   </td>
                        </tr>
                      
                      
                       @endforeach
                       
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              
            </div>
          </div>
        </div>
        </div>
       
        <!-- /page content -->

     
      </div>
    </div>

    

  </body>

</html>