<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Middleware;
use Maatwebsite\Excel\Excel;

class gtfsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getRoutes()
    {
        $client = new \GuzzleHttp\Client();
        $request = $client->get(''.config('global.base_url').'/api/v1/routes');
        $response = json_decode($request->getBody());
        $responses=$response->data;
    //    dd($response->data);
    
       return view('gtfs.routes')->with('responses', $responses); 
    }

    public function UploadRoutes(Request $request)
    {

            if ($request->hasFile('file')) {
                $path = $request->file('file')->getRealPath();
               // dd($path);
                $data = \Excel::load($path)->get();
        
                if ($data->count()) {
                    foreach ($data as $key => $value) {
                        $arr[] = [ 'agency_id' => $value->agency_id,
                                    'route_color' => $value->route_color,
                                    'route_desc' => "$value->route_desc",
                                    'route_id' => $value->route_id, 
                                    'route_long_name' => $value->route_long_name,
                                    'route_short_name' => $value->route_short_name,
                                    'route_text_color' => $value->route_text_color,
                                    'route_type' => $value->route_type
                                 ];
                    }
                


                        
                $n1=json_encode(array('routes'=>$arr));
                $n= json_decode($n1);
                $rep=collect($n)->toJson();
            
                //    dd($rep);
    
                         $jsonData =($rep);// response()->json($data);
                         $json_url = ''.config('global.base_url').'/api/v1/routes';
               //  dd($json_url);
                         $ch = curl_init( $json_url );
                         $options = array(
                             CURLOPT_RETURNTRANSFER => true,
                             CURLOPT_HTTPHEADER => array('Content-type: application/json') ,
                             CURLOPT_POSTFIELDS => $jsonData
                         );
                         curl_setopt_array( $ch, $options );
                         $result =  curl_exec($ch);
                      
    
                            // dd($result);
                            $statusCode= curl_getinfo($ch,CURLINFO_HTTP_CODE);
                            //   dd($statusCode);
                            if($statusCode==500){
                                return redirect()->back()->with('error','Please upload the required file');
                                curl_close($ch);
                            }elseif($statusCode==200){
                                return redirect()->back()->with('status','Data Uploaded Succesfully');
                                curl_close($ch);
                            }
                
            }
   
    }
    }

    public function deleteRoutes(){
        dd('delete');
    }

    public function deleteCalendars(){
        dd('delete calendars');
    }

    public function deleteTrips(){
        dd('delete trips');
    }

    public function deleteStops(){
        dd('delete stops');
    }

    public function deleteShapes(){
        dd('delete shapes');
    }
    //stops

    public function getStops()
    {
        $client = new \GuzzleHttp\Client();
        $request = $client->get(''.config('global.base_url').'/api/v1/stops');
        $response = json_decode($request->getBody());
        $responses=$response->data;
    //    dd($response->data);
    
       return view('gtfs.stops')->with('responses', $responses); 
    }

    public function UploadStops(Request $request)
    {

            if ($request->hasFile('file')) {
                $path = $request->file('file')->getRealPath();
               // dd($path);
                $data = \Excel::load($path)->get();
        
                if ($data->count()) {
                    foreach ($data as $key => $value) {
                        $arr[] = ['desc' => $value->stop_descr,
                                  'location_type' => $value->location_type,
                                  'parent_tation' => $value->parent_station,
                                  'stop_code' => $value->stop_code,
                                   'stop_id' => $value->stop_id, 
                                   'stop_lat' => $value->stop_lat,
                                  'stop_lon' => $value->stop_lon,
                                  'stop_name' => $value->stop_name
                                 ];
                    }
                  

                    $n1=json_encode(array('stops'=>$arr));
                    $n= json_decode($n1);
                    $rep=collect($n)->toJson();
                
        
        
                             $jsonData =($rep);// response()->json($data);
                             $json_url = ''.config('global.base_url').'/api/v1/stops';
                     
                             $ch = curl_init( $json_url );
                             $options = array(
                                 CURLOPT_RETURNTRANSFER => true,
                                 CURLOPT_HTTPHEADER => array('Content-type: application/json') ,
                                 CURLOPT_POSTFIELDS => $jsonData
                             );
                             curl_setopt_array( $ch, $options );
                             $result =  curl_exec($ch);
                          
        
                                // dd($err);
                                $statusCode= curl_getinfo($ch,CURLINFO_HTTP_CODE);
                                //    dd($statusCode);
                                if($statusCode==500){
                                    return redirect()->back()->with('error','Please upload the required file');
                                    curl_close($ch);
                                }elseif($statusCode==200){
                                    return redirect()->back()->with('status','Data Uploaded Succesfully');
                                    curl_close($ch);
                                }
                                            
                }
            }
   
    }




     //stop times

     public function getStopTimes()
     {
         $client = new \GuzzleHttp\Client();
         $request = $client->get(''.config('global.base_url').'stop-times'
                     ,array(
                        'timeout' => 500, // Response timeout
                        'connect_timeout' => 500, // Connection timeout
                        ));
                 //       dd($request);
         $resp = json_decode($request->getBody());
       //$responses=$response->data->take(7);
       $responses=collect($resp->data)->take(200);
     
        return view('gtfs.stop_times')->with('responses', $responses); 
     }
 
     public function uploadStopTimes(Request $request)
     {
 
             if ($request->hasFile('file')) {
                 $path = $request->file('file')->getRealPath();
                // dd($path);
                 $data = \Excel::load($path)->get();
      //   dd($data);
                 if ($data->count()) {
                     foreach ($data as $key => $value) {
                         $arr[] = [ 'arrival_time' => $value->arrival_time,
                                    'departure_time' => $value->departure_time,
                                    'drop_off_type' => $value->drop_off_type,
                                    'pickup_type' => $value->pickup_type,
                                    'shape_dist_traveled' => $value->shape_dist_traveled,
                                    'stop_id' => $value->stop_id,
                                    'stop_sequence' => $value->stop_sequence,
                                    'trip_id' => $value->trip_id
                                           
                                  ];
                     }
                     $n1=json_encode(array('stopTimes'=>$arr));
                     $n= json_decode($n1);
                     $rep=collect($n)->toJson();
                 
         
         
                              $jsonData =($rep);// response()->json($data);
                              $json_url = ''.config('global.base_url').'stop-times';
                      
                              $ch = curl_init( $json_url );
                              $options = array(
                                  CURLOPT_RETURNTRANSFER => true,
                                  CURLOPT_HTTPHEADER => array('Content-type: application/json') ,
                                  CURLOPT_POSTFIELDS => $jsonData
                              );
                              curl_setopt_array( $ch, $options );
                              $result =  curl_exec($ch);
                           
         
                                 // dd($err);
                                 $statusCode= curl_getinfo($ch,CURLINFO_HTTP_CODE);
                                 //    dd($statusCode);
                                 if($statusCode==500){
                                     return redirect()->back()->with('error','Please upload the required file');
                                     curl_close($ch);
                                 }elseif($statusCode==200){
                                     return redirect()->back()->with('status','Data Uploaded Succesfully');
                                     curl_close($ch);
                                 }
                                             
                 }
             }
    
     }





     //Trips

     public function getTrips()
     {
         $client = new \GuzzleHttp\Client();
         $request = $client->get(''.config('global.base_url').'trips');
         $response = json_decode($request->getBody());
         $responses=$response->data;
     //    dd($response->data);
     
        return view('gtfs.trips')->with('responses', $responses); 
     }
 
     public function UploadTrips(Request $request)
     {
 
             if ($request->hasFile('file')) {
                 $path = $request->file('file')->getRealPath();
                // dd($path);
                 $data = \Excel::load($path)->get();
         
                 if ($data->count()) {
                     foreach ($data as $key => $value) {
                         $arr[] = ['block_id' => $value->block_id, 
                                   'direction_id' => $value->direction_id,
                                   'route_id' => $value->route_id,
                                   'service_id' => $value->service_id,
                                   'shape_id' => $value->shape_id,
                                   'trip_headsign' => $value->trip_headsign,
                                   'trip_id' => $value->trip_id,     
                                  ];
                     }
                     $n1=json_encode(array('trips'=>$arr));
                     $n= json_decode($n1);
                     $rep=collect($n)->toJson();
                 
         
         
                              $jsonData =($rep);// response()->json($data);
                              $json_url = ''.config('global.base_url').'trips';
                      
                              $ch = curl_init( $json_url );
                              $options = array(
                                  CURLOPT_RETURNTRANSFER => true,
                                  CURLOPT_HTTPHEADER => array('Content-type: application/json') ,
                                  CURLOPT_POSTFIELDS => $jsonData
                              );
                              curl_setopt_array( $ch, $options );
                              curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 0 );
                              curl_setopt( $ch, CURLOPT_TIMEOUT, 400 );
                              $result =  curl_exec($ch);
                           
         
                                // dd($result);
                                 $statusCode= curl_getinfo($ch,CURLINFO_HTTP_CODE);
                                 //    dd($statusCode);
                                 if($statusCode==500){
                                     return redirect()->back()->with('error','Please upload the required file');
                                     curl_close($ch);
                                 }elseif($statusCode==200){
                                     return redirect()->back()->with('status','Data Uploaded Succesfully');
                                     curl_close($ch);
                                 }
                 }
             }
    
     }



     //shapes

     public function getShapes()
     {
         $client = new \GuzzleHttp\Client();
         $request = $client->get(''.config('global.base_url').'shapes');
         $response = json_decode($request->getBody());
         $responses=$response->data;
     //    dd($response->data);
     
        return view('gtfs.shapes')->with('responses', $responses); 
     }
 
     public function UploadShapes(Request $request)
     {
 
             if ($request->hasFile('file')) {
                 $path = $request->file('file')->getRealPath();
                // dd($path);
                 $data = \Excel::load($path)->get();
         
                 if ($data->count()) {
                     foreach ($data as $key => $value) {
                         $arr[] = [ 'shape_dist_traveled' => $value->shape_dist_traveled,
                                   'shape_id' => $value->shape_id, 
                                   'shape_pt_lat' => $value->shape_pt_lat,
                                   'shape_pt_lon' => $value->shape_pt_lon,
                                   'shape_pt_sequence' => $value->shape_pt_sequence                    
                                  ];
                     }
                     $n1=json_encode(array('shapes'=>$arr));
                     $n= json_decode($n1);
                     $rep=collect($n)->toJson();
                 
         
         
                              $jsonData =($rep);// response()->json($data);
                              $json_url = ''.config('global.base_url').'shapes';
                      
                              $ch = curl_init( $json_url );
                              $options = array(
                                  CURLOPT_RETURNTRANSFER => true,
                                  CURLOPT_HTTPHEADER => array('Content-type: application/json') ,
                                  CURLOPT_POSTFIELDS => $jsonData
                              );
                              curl_setopt_array( $ch, $options );
                              $result =  curl_exec($ch);
                           
         
                                // dd($result);
                                 $statusCode= curl_getinfo($ch,CURLINFO_HTTP_CODE);
                                 //    dd($statusCode);
                                 if($statusCode==500){
                                     return redirect()->back()->with('error','Please upload the required file');
                                     curl_close($ch);
                                 }elseif($statusCode==200){
                                     return redirect()->back()->with('status','Data Uploaded Succesfully');
                                     curl_close($ch);
                                 }
                 }
             }
    
     }





     //Calendar

     public function getCalendar()
     {
         $client = new \GuzzleHttp\Client();
         $request = $client->get(''.config('global.base_url').'calendar');
         $response = json_decode($request->getBody());
         $responses=$response->data;
     //    dd($response->data);
     
        return view('gtfs.calendar')->with('responses', $responses); 
     }
 
     public function UploadCalendar(Request $request)
     {
 
             if ($request->hasFile('file')) {
                 $path = $request->file('file')->getRealPath();
                // dd($path);
                 $data = \Excel::load($path)->get();
         
                 if ($data->count()) {
                     foreach ($data as $key => $value) {
                         $arr[] = ['end_date' => $value->end_date,
                                   'friday' => $value->friday,
                                   'monday' => $value->monday,
                                   'saturday' => $value->saturday,
                                   'service_id' => $value->service_id, 
                                   'start_date' => $value->start_date,
                                   'sunday' => $value->sunday,
                                   'thursday' => $value->thursday,
                                   'tuesday' => $value->tuesday,
                                   'wednesday' => $value->wednesday
                                  ];
                     }
              
                $n1=json_encode(array('calendar'=>$arr));
            $n= json_decode($n1);
            $rep=collect($n)->toJson();
        


                     $jsonData =($rep);// response()->json($data);
                     $json_url = ''.config('global.base_url').'calendar';
             
                     $ch = curl_init( $json_url );
                     $options = array(
                         CURLOPT_RETURNTRANSFER => true,
                         CURLOPT_HTTPHEADER => array('Content-type: application/json') ,
                         CURLOPT_POSTFIELDS => $jsonData
                     );
                     curl_setopt_array( $ch, $options );
                     $result =  curl_exec($ch);
                  

                        // dd($err);
                        $statusCode= curl_getinfo($ch,CURLINFO_HTTP_CODE);
                        //    dd($statusCode);
                        if($statusCode==500){
                            return redirect()->back()->with('error','Please upload the required file');
                            curl_close($ch);
                        }elseif($statusCode==200){
                            return redirect()->back()->with('status','Data Uploaded Succesfully');
                            curl_close($ch);
                        }
                                            


                             }
             }
    
     }




     //Calendar dates

     public function getCalendarDates()
     {
         $client = new \GuzzleHttp\Client();
         $request = $client->get(''.config('global.base_url').'calendar-dates');
         $response = json_decode($request->getBody());
         $responses=$response->data;
     //    dd($response->data);
     
        return view('gtfs.calendar_dates')->with('responses', $responses); 
     }
 
     public function UploadCalendarDates(Request $request)
     {
 
             if ($request->hasFile('file')) {
                 $path = $request->file('file')->getRealPath();
                // dd($path);
                 $data = \Excel::load($path)->get();
         
                 if ($data->count()) {
                     foreach ($data as $key => $value) {
                         $arr[] = ['service_id' => $value->service_id, 
                                   'date' => $value->date,
                                   'exception_type' => $value->exception_type     
                                  ];
                     }
                     dd($arr);
                     if (!empty($arr)) {
                         DB::table('template')->insert($arr);
         
                         return "Success";
                     }
                 }
             }
    
     }



     //agency

     public function getAgency()
     {
         $client = new \GuzzleHttp\Client();
         $request = $client->get(''.config('global.base_url').'agency');
         $response = json_decode($request->getBody());
         $responses=$response->data;
     //    dd($response->data);
     
        return view('gtfs.agency')->with('responses', $responses); 
     }
 
     public function UploadAgency(Request $request)
     {
 
             if ($request->hasFile('file')) {
                 $path = $request->file('file')->getRealPath();
                // dd($path);
                 $data = \Excel::load($path)->get();
         
                 if ($data->count()) {
                     foreach ($data as $key => $value) {
                         $arr[] = ['agency_id' => $value->agency_id, 
                                   'agency_name' => $value->agency_name,
                                   'agency_url' => $value->agency_url,
                                   'agency_timezone' => $value->agency_timezone,
                                   'agency_lang' => $value->agency_lang,
                                   'agency_phone' => $value->agency_phone                           
                                  ];
                     }
                     dd($arr);
                     if (!empty($arr)) {
                         DB::table('template')->insert($arr);
         
                         return "Success";
                     }
                 }
             }
    
     }
    
}
