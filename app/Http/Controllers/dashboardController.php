<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class dashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){

        //get disruption
        $client = new \GuzzleHttp\Client();
        $request = $client->get(env('BASE_API_URL').'/service-disruptions');
        $response = json_decode($request->getBody());
        $disrup=$response->data;
        $disruption=collect($disrup)->sortBy('createdAt', SORT_REGULAR, true)->take(3);

        //get information
        $client = new \GuzzleHttp\Client();
        $request = $client->get(env('BASE_API_URL').'/information');
        $response = json_decode($request->getBody());
        $info=$response->data;
        $information=collect($info)->sortBy('createdAt', SORT_REGULAR, true)->take(3);
        //get incidence
        $client = new \GuzzleHttp\Client();
        $request = $client->get(env('BASE_API_URL').'/incidents');
        $response = json_decode($request->getBody());
        $incid=$response->data;
        $incidence=collect($incid)->sortBy('createdAt', SORT_REGULAR, true)->take(3);

        //get faq
        $client = new \GuzzleHttp\Client();
        $request = $client->get(env('BASE_API_URL').'/faq');
        $response = json_decode($request->getBody());
        $faq=$response->data;

        //Counter
        $infoCount=count($info);
        $disCount=count($disrup);
        $incCount=count($incid);
        $faqCount=count($faq);
    
       return view('welcome')->with('disruption', $disruption)
                             ->with('information', $information)
                             ->with('incidence', $incidence)
                             ->with('infoCount', $infoCount)
                             ->with('disCount', $disCount)
                             ->with('faqCount', $faqCount)
                             ->with('incCount', $incCount); 
      
    }
}
