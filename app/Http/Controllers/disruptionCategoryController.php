<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class disruptionCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    //view
    public function index(){
        $category=DB::table('disruption_category')->get();
        return view('disruption-category')->with('category',$category);
    }
       

       
          public function addDisruptionCategory(Request $request){
    //    dd($request->all());
              DB::table('disruption_category')->insert([
                'category_name'=>$request->category
              ]);
       
             return redirect()->back()->with('status','New Category Added');
          }
       
       
             public function editDisruptionCategory(Request $request,$id){
                // dd($request->all());
              DB::table('disruption_category')->where('id',$id)->update([
                'category_name'=>$request->category,
                'status'=>$request->status
              ]);
       
              return redirect()->back()->with('status','Category Updated Successfully');
          }
       
}
