<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use DB;

class informationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function getInfo()
{
    $client = new \GuzzleHttp\Client();
   // $request = $client->get('http://154.118.230.227:9093/api/v1/information');

   $request = $client->get(''.config('global.base_url').'/api/v1/information');
    $response = json_decode($request->getBody());
    $responses=$response->data;
//    dd($response->data);
$cat=DB::table('information_category')->where('status','Active')->get();
   return view('information')->with('responses', $responses)
                               ->with('cat', $cat);



}

public function getInfoAdd(Request $request)
{
    $title=$request->title;
    $category=$request->category;
    $content=$request->content;
    $type=$request->type;
   // dd($request->all());
$client = new \GuzzleHttp\Client();
// $url = "http://154.118.230.227:9093/api/v1/information";

$url = ''.config('global.base_url').'/api/v1/information';
$request = $client->post($url, [
    'json' => [
        'title'=>$title,'category'=>$category,'content'=>$content,'type'=>$type
       ]
   ]);
// $request->getBody();

if($request->getBody()->getContents()){
    return redirect()->back()->with('status','Information Added');
    }else
    return redirect()->back()->with('error','Cant Add Information');
}

public function getInfoEdit(Request $request,$id)
{
//dd($id);

$title=$request->title;
$category=$request->category;
$content=$request->content;
$status=$request->status;
//$type=$request->type;
//dd($request->all());
$client = new \GuzzleHttp\Client();
// $url = "http://154.118.230.227:9093/api/v1/information/$id";
$url = ''.config('global.base_url').'/api/v1/information/'.$id.'';

$request = $client->put($url, [
'json' => [
    'title'=>$title,'category'=>$category,'content'=>$content ,'status'=>$status
   ]
]);
// $request->getBody();


if($request->getBody()->getContents()){
  
return redirect()->back()->with('status','Information Updated');
    }else
    return redirect()->back()->with('error','Cant Update Information');


}
}
