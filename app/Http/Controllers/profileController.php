<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Hash;

class profileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $id=Auth::user()->id;
   $user=DB::table('users')->where('id',$id)->first();
        return view('profile')->with('user',$user);
    }



    public function editProfile(request $request,$id)
    {
        // $validator=  $request->validate([
        //     'newpassword' => 'required|string|min:6|confirmed',
        // ]);
        //  dd($request->all());

         $request->validate([
                'currentPassword' => ['required', new MatchOldPassword],
                'newPassword' => ['required'],
                'confirmPassword' => ['same:newPassword'],
            ]);
        DB::table('users')->where('id',$id)->update([
            'password' =>Hash::make($request->newPassword),
            
        ]);




        // $request->validate([
        //     'currentPassword' => ['required', new MatchOldPassword],
        //     'newPassword' => ['required'],
        //     'confirmPassword' => ['same:newPassword'],
        // ]);
   
        // User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);
   
        //dd('Password change successfully.');

        return redirect()->back()->with('status','Password Updated');
    }
}
