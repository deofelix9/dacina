<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class serviceDisruptionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        $client = new \GuzzleHttp\Client();
        $request = $client->get(''.config('global.base_url').'/api/v1/service-disruptions');

        
        $response = json_decode($request->getBody());
        $responses=$response->data;
    //    dd($response->data);
    $dis_cat=DB::table('disruption_category')->where('status','Active')->get();
    
       return view('disruption')->with('responses', $responses)
                                ->with('dis_cat', $dis_cat); 
      
    }


    public function disruptionAdd(Request $request)
{
    $title=$request->title;
    $category=$request->category;
    $content=$request->content;
    $summary=$request->summary;
    $location=$request->location;
   // $type='test';
    // dd($request->all());
$client = new \GuzzleHttp\Client();
// $url = "http://154.118.230.227:9093/api/v1/service-disruptions";
$url = ''.config('global.base_url').'/api/v1/service-disruptions';

$request = $client->post($url, [
    'json' => [
        'title'=>$title,'category'=>$category,'information'=>$content,'location'=>$location,'summary'=>$summary
       ]
   ]);
//dd($request->getBody()->getContents());
if($request->getBody()->getContents()){
return redirect()->back()->with('status','Service Disruption Added');
}else
return redirect()->back()->with('error','Cant Add Service Disruption');
}



    public function disruptionEdit(Request $request,$id)
{
    $title=$request->title;
    $category=$request->category;
    $content=$request->content;
    $summary=$request->summary;
    $location=$request->location;
    $status=$request->status;
// dd($request->all());
$client = new \GuzzleHttp\Client();
$url = ''.config('global.base_url').'/api/v1/service-disruptions/'.$id.'';

$request = $client->put($url, [
'json' => [
    'title'=>$title,'category'=>$category,'information'=>$content,'location'=>$location,'summary'=>$summary,'status'=>$status
   ]
]);
// $request->getBody();



//dd($request->getBody()->getContents());
if($request->getBody()->getContents()){
    return redirect()->back()->with('status','Service Disruption Updated');
    }else{
    return redirect()->back()->with('error','Cant Edit Service Disruption');
    }


}
}
