<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service\MsdgApi;
use Mail;

class contactusController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){

        $client = new \GuzzleHttp\Client();
        $request = $client->get(''.config('global.base_url').'/api/v1/messages');
        
        
        $response = json_decode($request->getBody());
        $responses=$response->data;
    //    dd($response->data);
    
       return view('contact-us')->with('responses', $responses); 
      
    }


    public function reply(Request $request,$id){
      //  dd($request->all());
 $phone1=$request->phone;
 $email=$request->email;
$phone='255'. (int) $phone1;
       // if(substr($phone,0,1)==0)
     //  dd( str_replace(0,255,(int) $phone) );
     //  else
     //  dd($phone);
        // dd($request->all();
        // DB::table('reply')->insert([
        //     'suggestion_id' => $id,
        //     'reply' =>$request->message
        // ]);
    
        // DB::table('suggestion')->where('id',$id)->update([
        //     'status' => 'Replied',
        //     'level' => $request->level?$request->level:'Normal',
        //     'display' => $request->display?$request->display:'Private'
        // ]);
    
    
         //Send email
        
         if(isset($email)){
            Mail::send('email-reply',
             array('email'=>$email,'reply'=>$request->message), function($message) use($email,$request)
      {
             $message->from('info@dart.go.tz');
             $message->to($email, '')->subject('WAKALA WA MABASI YAENDAYO HARAKA');
         });
      }
      
     
    
    //  //Send sms
    
$message = strip_tags($request->message);
$msdg = new Msdgapi();
$datetime = date('Y-m-d H:i:s');
$message  = array('message' => $message,'datetime'=>$datetime, 'sender_id'=>'DART', 	
	'mobile_service_id'=>'192', 'recipients'=>$phone);
$json_data = json_encode($message);
$msdg->sendQuickSms(array('data'=>$json_data,'datetime'=>$datetime));
      //end test sending smss







$status=1;
$client = new \GuzzleHttp\Client();
$url = ''.config('global.base_url').'/api/v1/messages/'.$id.'/'.$status.'';
$request = $client->put($url, [
'json' => [
   'status'=>$status
   ]
]);


// if($request->getBody()->getContents()){
  
// return redirect()->back()->with('status','Information Updated');
//     }else
//     return redirect()->back()->with('error','Cant Update Information');


// }





    
        return redirect()->back()->with('status','Successfully replied');
    }
    
}
