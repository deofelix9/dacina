<?php
namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Log;

class BrtNetworkController extends Controller
{
    private $_routes = [];
    private $_points = [];
    private $apiClient;
    private $baseUrl;
    public $_routeId;

    public function __construct()
    {
        $this->middleware('auth');
        $this->apiClient = new \GuzzleHttp\Client();
        $this->baseUrl = env('BASE_API_URL_ROOT');
    }

    public function index()
    {
        $client = new \GuzzleHttp\Client();
        $request = $client->get(env('BASE_API_URL') . "/routes?status=1");
        $routes = json_decode($request->getBody());
        $this->_routes = $routes->data;
        Log::info("FOUUND ");
        return view('brt.lanes')->with('routes', $this->_routes)->with("points", $this->_points);
    }

    public function byRoute(Request $request)
    {

        if (!isset($request->routeId)) {
            dd($request->all);
        }
        $this->_routeId = $request->routeId;
        $client = new \GuzzleHttp\Client();
        //brt-network?direction=1&routeId=17
        $url = env('BASE_API_URL_ROOT') . "/api/brt-network?direction=1&routeId=" . $request->routeId;
        $request = $client->get($url);
        $points = json_decode($request->getBody());
        $this->_points = $points->data;
        if (count($points->data) == 0) {
            return redirect()->back()->with('status', "Not points for this route");
        }
        return view('brt.lanes-view')->with("points", $this->_points)->with('routes', $points)->with('routeId',$this->_routeId);
    }

    public function viewByRoute(){
        return view('brt.lanes-view')->with("points", $this->_points)->with('routes', $this->_points);
    }

    public function saveBrt(Request $request)
    {

        $response = $this->apiClient->post($this->baseUrl."/api/brt-network", ["json" => $request->all()]);
        $response  = json_decode($response->getBody());
        if($response->success === true){
            return redirect()->route('brt-network')->with('status',$response->message);
        }else{
            return redirect()->route('brt-network')->with('error',$response->message); 
        }

    }

    public function updateSequence(Request $request)
    {
        $response = $this->apiClient->put($this->baseUrl."/api/update-sequence", ["json" => [
            "id" => $request->id,
            "sequence" => $request->sequence,
        ]]);
        $response  = json_decode($response->getBody());
        if($response->success === true){
            return redirect()->back()->with('status',$response->message);
        }else{
            return redirect()->back()->with('error',$response->message); 
        }
    }
}
