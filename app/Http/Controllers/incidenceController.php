<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service\MsdgApi;

class incidenceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){

        $client = new \GuzzleHttp\Client();
        $request = $client->get(''.config('global.base_url').'/api/v1/incidents');
        // $request = $client->get('http://192.168.0.58:8001/api/v1/incidents');
        
        $response = json_decode($request->getBody());
        $responses=$response->data;
    //    dd($response->data);
    
       return view('incidence')->with('responses', $responses); 
      
    }

    public function reply(Request $request,$id){
        //  dd($request->all());
   $phone1=$request->phone;
   //$email=$request->email;
  $phone='255'. (int) $phone1;
     
      
      
           //Send email
          
        //    if(isset($email)){
        //       Mail::send('email-reply',
        //        array('email'=>$email,'reply'=>$request->message), function($message) use($email,$request)
        // {
        //        $message->from('info@dart.go.tz');
        //        $message->to($email, '')->subject('WAKALA WA MABASI YAENDAYO HARAKA');
        //    });
        // }
        
       
      
       //Send sms
    
        $message = strip_tags($request->message);
        
        $msdg = new Msdgapi();
        $datetime = date('Y-m-d H:i:s');
        $message  = array('message' => $message,'datetime'=>$datetime, 'sender_id'=>'DART', 	
            'mobile_service_id'=>'192', 'recipients'=>$phone);
        $json_data = json_encode($message);
        $msdg->sendQuickSms(array('data'=>$json_data,'datetime'=>$datetime));
              //end test sending smss
       //End sms
      
          return redirect()->back()->with('status','Successfully replied');
      }
      
}
