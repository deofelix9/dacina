<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class informationCategoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    //view
    public function index(){
        $category=DB::table('information_category')->get();
        return view('info-category')->with('category',$category);
    }
       

       
          public function addCategory(Request $request){
       
              DB::table('information_category')->insert([
                'category_name'=>$request->category
              ]);
       
             return redirect()->back()->with('status','New Category Added');
          }
       
       
          public function editCategory(Request $request,$id){
            // dd($request->all());
          DB::table('information_category')->where('id',$id)->update([
            'category_name'=>$request->category,
            'status'=>$request->status
          ]);
   
          return redirect()->back()->with('status','Category Updated Successfully');
      }
}
