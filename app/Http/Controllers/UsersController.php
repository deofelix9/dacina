<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){

    	 $users = DB::table('users')->get();

    	return view('users')->with('users',$users);

    	
    }



 public function addUsers(request $request)
    {

// dd($request->all());
              $validator=  $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255' //|unique:users,email',
           // 'password' => 'required|string|min:6|confirmed',
        ]);


           
      
$emailExist=DB::table('users')->where('email',$request->email)->exists();
        if($emailExist){
            return redirect()->back()->with('exist','The user with the E-mail '.$request->email.' already registered');
        }else{

        $users=DB::table('users')->insert([
            'email' =>$request->email,
            'name' =>$request->name,
            'role' =>$request->role,
            'password' =>bcrypt('dart@2016'),
            
        ]);

        return redirect()->back()->with('status','New User '.$request->name.' Added');
        }
    }



 public function editUsers(request $request,$id)
    {
        
        DB::table('users')->where('id',$id)
        ->update([

            'emp_id'=>$request->fullName,
            'email' =>$request->username,
            'role_id' =>$request->role,
            'branch_id' =>$request->branch,
            'site_id'=>$request->site,
            'project_id' =>$request->project,
            'password' =>bcrypt($request->password),
            
        ]);

        return redirect()->back()->with('status','User '.$request->name.' Updated');
    }


    public function editUserRole(request $request,$id)
    {
        
        DB::table('users')->where('id',$id)
        ->update([

            'role' =>$request->role
            
        ]);

        return redirect()->back()->with('status','User Role for  Updated');
    }


 public function Activate($id)
    {
        DB::table('users')->where('id',$id)->update([
            'status' =>'Active'
        ]);
        return redirect()->back()->with('status','User  Activated');
    }

    public function DeActivate($id)
    {
        DB::table('users')->where('id',$id)->update([
            'status' =>'InActive'
        ]);
        return redirect()->back()->with('status','User  De-activated');
    }


        public function deleteUsers($id){
        DB::table('users')->where('id',$id)->delete();
       
         return redirect()->back()->with('status','Users Permanently Deleted');
    }

    public function resetPassword($id){
        DB::table('users')->where('id',$id)->update([
            'password' =>bcrypt('dart@2016')
        ]);
        return redirect()->back()->with('status','User Password for Reseted');
    }
}
