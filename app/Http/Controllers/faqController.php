<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

class faqController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function getFaq()
{
   
    //dd(config('global.base_url'));
    $client = new \GuzzleHttp\Client();

    $request = $client->get(''.config('global.base_url').'/api/v1/faq');
    $response = json_decode($request->getBody());
    $responses=$response->data;
//    dd($response->data);

   return view('faq')->with('responses', $responses); 



}

public function getfaqAdd(Request $request)
{
    $question=$request->question;
    $answer=$request->answer;
     
   // dd($request->all());
$client = new \GuzzleHttp\Client();
$url = ''.config('global.base_url').'/api/v1/faq';

$request = $client->post($url, [
    'json' => [
        'question'=>$question,'answer'=>$answer
       ]
   ]);
// $request->getBody();
return redirect()->back()->with('status','FAQ Added Successfully');
}

public function getfaqEdit(Request $request,$id)
{
    //dd($id);

$question=$request->question;
$answer=$request->answer;
$status=$request->status;
//dd($answer);
//  dd($request->all());
$client = new \GuzzleHttp\Client();
$url = ''.config('global.base_url').'/api/v1/faq/'.$id.'';



//dd($url);
$request = $client->put($url, [
'json' => [
    'question'=>$question,'answer'=>$answer,'status'=>$status
   ]
]);
// $request->getBody();

//dd($request->getBody()->getContents());

if($request->getBody()->getContents()){
    return redirect()->back()->with('status','FAQ Updated Successfully');
    }else
    return redirect()->back()->with('error','Cant Update FAQ');

}
}
